package com.lytfly.demo.listener;

import com.lytfly.demo.account.service.IFlowInfoService;
import com.lytfly.demo.utils.SpringContextUtil;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.springframework.stereotype.Component;

/**
 * @author liuyuantao
 */
@Slf4j
@Component
public class MyTaskListener implements TaskListener {

    @Override
    public void notify(DelegateTask delegateTask) {
        log.info("eventName=={}", delegateTask.getEventName());
        if (delegateTask.getEventName().equals("assignment")) {
            IFlowInfoService flowService = (IFlowInfoService) SpringContextUtil.getBean("flowService");
            log.info("flowService={}", flowService);
            flowService.createTaskEvent(delegateTask);
        }
    }
}
