package com.lytfly.demo.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author liuyuantao
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result {
    private Integer status;
    private String message;
}
