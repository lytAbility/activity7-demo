package com.lytfly.demo.account.controller;


import cn.hutool.core.collection.CollUtil;
import com.lytfly.demo.account.entity.Evection;
import com.lytfly.demo.account.service.IEvectionService;
import com.lytfly.demo.account.service.impl.ActFlowCommService;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.runtime.ProcessInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author liuyuantao
 * @since 2021年07月10日 16时22分
 */
@RestController
@RequestMapping("/account/evection")
@Slf4j
public class EvectionController {
    @Autowired
    private IEvectionService evectionService;

    /**
     * 查询本人的出差申请
     *
     * @param request
     * @return
     */
    @GetMapping("/findAll")
    public List<Evection> findAll(HttpServletRequest request) {
        Integer userId = (Integer) request.getSession().getAttribute("userid");
        return evectionService.lambdaQuery().eq(Evection::getUserId, userId).list();
    }

    @GetMapping("/evection/{id}")
    public Evection findAll(@PathVariable(name = "id") Long evectionId) {
        return evectionService.getById(evectionId);
    }

    @Autowired
    private ActFlowCommService actFlowCommService;

    /**
     * 新增出差申请
     *
     * @param request
     * @param evection
     */
    @PostMapping("/add")
    public void addEvection(HttpServletRequest request, @RequestBody Evection evection) {
        Integer userId = (Integer) request.getSession().getAttribute("userid");
        evection.setUserId(userId);
        evection.setState(0);
        evection.insert();
        Integer evectionId = evection.getId();
        String formKey = "evection";
        String beanName = formKey + "Service";
        //使用流程变量设置字符串（格式 ： Evection:Id 的形式）
        //使用正在执行对象表中的一个字段BUSINESS_KEY(Activiti提供的一个字段)，让启动的流程（流程实例）关联业务
        String businessKey = formKey + ":" + evectionId;
        ProcessInstance processInstance = actFlowCommService.startProcess(formKey, beanName, businessKey, evectionId);
        //流程实例ID
        String processDefinitionId = processInstance.getProcessDefinitionId();
        log.info("processDefinitionId is {}", processDefinitionId);
        List<Map<String, Object>> taskList = actFlowCommService.myTaskList(userId.toString());
        if (!CollUtil.isEmpty(taskList)) {
            for (Map<String, Object> map : taskList) {
                if (map.get("assignee").toString().equals(userId.toString()) &&
                        map.get("processDefinitionId").toString().equals(processDefinitionId)) {
                    log.info("processDefinitionId is {}", map.get("processDefinitionId").toString());
                    log.info("taskid is {}", map.get("taskid").toString());
                    actFlowCommService.completeProcess("同意", map.get("taskid").toString(), userId.toString());
                }

            }
        }

    }

}
