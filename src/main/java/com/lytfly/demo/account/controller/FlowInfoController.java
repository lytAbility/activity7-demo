package com.lytfly.demo.account.controller;


import com.lytfly.demo.account.entity.FlowInfo;
import com.lytfly.demo.account.service.IFlowInfoService;
import com.lytfly.demo.account.service.impl.ActFlowCommService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author liuyuantao
 * @since 2021年07月10日 16时22分
 */
@RestController
@RequestMapping("/account/flowInfo")
public class FlowInfoController {

    @Autowired
    private IFlowInfoService flowInfoService;
    @Autowired
    private ActFlowCommService actFlowCommService;


    /**
     * 查询所有流程
     *
     * @return
     */
    @GetMapping("/findAll")
    public List<FlowInfo> findAllFlow() {
        return flowInfoService.lambdaQuery().orderByDesc(FlowInfo::getCreateTime).list();
    }

    /**
     * 部署流程
     *
     * @param request
     * @return 0-部署失败  1- 部署成功  2- 已经部署过
     */
    @PutMapping("/deployment/{id}")
    @ResponseBody
    public Integer deployment(HttpServletRequest request, @PathVariable(name = "id") Long id) {
        FlowInfo flowInfo = flowInfoService.getById(id);
        if (flowInfo.getState() == 1) {
            return 2;
        }
        actFlowCommService.saveNewDeploy(flowInfo);
        flowInfo.setState(1);
        flowInfo.updateById();
        return 1;
    }


    /**
     * 查询用户任务
     *
     * @param request
     * @return
     */
    @GetMapping("/findUserTask")
    public List<Map<String, Object>> findUserTask(HttpServletRequest request) {
        Integer userId = (Integer) request.getSession().getAttribute("userid");
        return flowInfoService.findUserTask(userId);
    }

    /**
     * 查询任务详细信息
     *
     * @param request
     * @return
     */
    @GetMapping("/findTaskInfo")
    public List<Map<String, Object>> findTaskInfo(HttpServletRequest request) {
        Integer userId = (Integer) request.getSession().getAttribute("userid");
        return flowInfoService.findTaskInfo(userId);
    }

    /**
     * 完成任务
     *
     * @param request
     */
    @PutMapping("/completeTask/{taskId}")
    public void completeTask(HttpServletRequest request, @PathVariable("taskId") String taskId) {
        Integer userId = (Integer) request.getSession().getAttribute("userid");
        flowInfoService.completeTask(taskId, userId);
    }

    /**
     * 查询
     *
     * @return
     */
    @GetMapping("/findFlowTask/{id}")
    public Map<String, Object> findFlowTask(@PathVariable(name = "id") Long id) {
        String businessKey = "evection:" + id;
        actFlowCommService.searchHistory(businessKey);
        return null;
    }

}
