package com.lytfly.demo.account.controller;


import com.lytfly.demo.account.entity.SiteMessage;
import com.lytfly.demo.account.service.ISiteMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author liuyuantao
 * @since 2021年07月10日 16时21分
 */
@RestController
@RequestMapping("/account/site-message")
public class SiteMessageController {

    @Autowired
    private ISiteMessageService siteMessageService;

    /**
     * 查询所有站内消息
     *
     * @return
     */
    @GetMapping("/msg/findAll")
    public List<SiteMessage> findAll(HttpServletRequest request) {
        Integer userId = (Integer) request.getSession().getAttribute("userid");
        return siteMessageService.lambdaQuery().eq(SiteMessage::getUserId, userId).list();
    }

    @GetMapping("/msg/findOne/{id}")
    public SiteMessage findOne(@PathVariable(name = "id") Long id,
                               HttpServletRequest request) {
        Integer userId = (Integer) request.getSession().getAttribute("userid");
        return siteMessageService.lambdaQuery().eq(SiteMessage::getUserId, userId)
                .eq(SiteMessage::getId, id)
                .getEntity();
    }

    /**
     * 修改消息
     *
     * @param id
     */
    @PutMapping("/msg/{id}")
    public void readMsg(@PathVariable(name = "id") Integer id, HttpServletRequest request) {
        Integer userId = (Integer) request.getSession().getAttribute("userid");
        siteMessageService.lambdaUpdate()
                .eq(SiteMessage::getUserId, userId)
                .eq(SiteMessage::getId, id)
                .set(SiteMessage::getIsRead, 1).update();
    }

}
