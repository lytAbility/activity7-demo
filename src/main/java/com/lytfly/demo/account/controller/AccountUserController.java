package com.lytfly.demo.account.controller;


import com.lytfly.demo.account.entity.AccountUser;
import com.lytfly.demo.account.service.IAccountUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author liuyuantao
 * @since 2021年07月10日 16时20分
 */
@RestController
@RequestMapping("/account/user")
public class AccountUserController {

    @Autowired
    private IAccountUserService accountUserService;

    /**
     * 查询用户列表
     *
     * @return
     */
    @GetMapping("/findAll")
    public List<AccountUser> findUserList() {
        return accountUserService.list();
    }

    /**
     * 查询单个用户
     *
     * @param userId
     * @return
     */
    @GetMapping("/findOne/{id}")
    public AccountUser findOneUser(@PathVariable(name = "id") Long userId) {
        return accountUserService.getById(userId);
    }

}
