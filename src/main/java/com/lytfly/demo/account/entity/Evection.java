package com.lytfly.demo.account.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;
import java.time.LocalDate;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.lytfly.demo.config.LocalDateTime4TimestampDeserializer;
import com.lytfly.demo.config.LocalDateTime4TimestampSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author liuyuantao
 * @since 2021年07月10日 16时22分
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("evection")
public class Evection extends Model<Evection> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("user_id")
    private Integer userId;

    /**
     * 出差申请单名称
     */
    @TableField("evection_name")
    private String evectionName;

    private Double num;

    /**
     * 预计开始时间
     */
    @TableField("begin_date")
    @JsonDeserialize(using = LocalDateTime4TimestampDeserializer.class)
    @JsonSerialize(using = LocalDateTime4TimestampSerializer.class)
    private LocalDate beginDate;

    /**
     * 预计结束时间
     */
    @TableField("end_date")
    @JsonDeserialize(using = LocalDateTime4TimestampDeserializer.class)
    @JsonSerialize(using = LocalDateTime4TimestampSerializer.class)
    private LocalDate endDate;

    /**
     * 目的地
     */
    private String destination;

    private String reason;

    /**
     * 0-初始录入   1-开始审批     2-审批完成
     */
    private Integer state;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
