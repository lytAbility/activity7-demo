package com.lytfly.demo.account.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.lytfly.demo.config.LocalDateTime4TimestampDeserializer;
import com.lytfly.demo.config.LocalDateTime4TimestampSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author liuyuantao
 * @since 2021年07月10日 16时21分
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("site_message")
public class SiteMessage extends Model<SiteMessage> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("user_id")
    private Integer userId;

    /**
     * 消息类型  1-代办任务
     */
    @TableField("`type`")
    private Integer type;

    @TableField("content")
    private String content;

    @TableField("task_id")
    private String taskId;

    /**
     * 是否已读  0- 已读  1-未读
     */
    @TableField("is_read")
    private Integer isRead;

    @TableField(value = "create_time", fill = FieldFill.INSERT)
    @JsonDeserialize(using = LocalDateTime4TimestampDeserializer.class)
    @JsonSerialize(using = LocalDateTime4TimestampSerializer.class)
    private LocalDateTime createTime;

    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    @JsonDeserialize(using = LocalDateTime4TimestampDeserializer.class)
    @JsonSerialize(using = LocalDateTime4TimestampSerializer.class)
    private LocalDateTime updateTime;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
