package com.lytfly.demo.account.mapper;

import com.lytfly.demo.account.entity.Evection;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liuyuantao
 * @since 2021年07月10日 16时22分
 */
public interface EvectionMapper extends BaseMapper<Evection> {

}
