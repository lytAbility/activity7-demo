package com.lytfly.demo.account.mapper;

import com.lytfly.demo.account.entity.AccountUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liuyuantao
 * @since 2021年07月10日 16时20分
 */
public interface AccountUserMapper extends BaseMapper<AccountUser> {

}
