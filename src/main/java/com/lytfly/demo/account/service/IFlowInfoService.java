package com.lytfly.demo.account.service;

import com.lytfly.demo.account.entity.FlowInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import org.activiti.engine.delegate.DelegateTask;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author liuyuantao
 * @since 2021年07月10日 16时22分
 */
public interface IFlowInfoService extends IService<FlowInfo> {

    /**
     * 查询用户任务
     *
     * @param userId
     * @return
     */
    public List<Map<String, Object>> findUserTask(Integer userId);


    /**
     * 完成任务
     *
     * @param userId
     */
    public void completeTask(String taskId, Integer userId);

    public void createTaskEvent(DelegateTask delegateTask);

    /**
     * 查询任务详细信息
     *
     * @param userId
     * @return
     */
    public List<Map<String, Object>> findTaskInfo(Integer userId);

}
