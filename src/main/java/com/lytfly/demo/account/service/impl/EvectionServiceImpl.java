package com.lytfly.demo.account.service.impl;

import com.lytfly.demo.account.entity.Evection;
import com.lytfly.demo.account.mapper.EvectionMapper;
import com.lytfly.demo.account.service.IEvectionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author liuyuantao
 * @since 2021年07月10日 16时22分
 */
@Service
public class EvectionServiceImpl extends ServiceImpl<EvectionMapper, Evection> implements IEvectionService {

    @Override
    public Map<String, Object> setvariables(Integer id) {
        Evection evection = this.getById(id);
        //设置流程变量
        Map<String, Object> variables = new HashMap<>();
        variables.put("assignee0", 1);
        variables.put("assignee1", 2);
        variables.put("assignee2", 3);
        variables.put("assignee3", 4);
        variables.put("evection", evection);
        return variables;
    }

    @Override
    public void startRunTask(Integer id) {
        Evection evection = new Evection();
        evection.setId(id);
        evection.setState(1);
        evection.updateById();
    }

    @Override
    public void endRunTask(Integer id) {
        Evection evection = new Evection();
        evection.setId(id);
        evection.setState(2);
        evection.updateById();
    }
}
