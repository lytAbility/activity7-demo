package com.lytfly.demo.account.service.impl;

import com.lytfly.demo.account.entity.AccountUser;
import com.lytfly.demo.account.mapper.AccountUserMapper;
import com.lytfly.demo.account.service.IAccountUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author liuyuantao
 * @since 2021年07月10日 16时20分
 */
@Service
public class AccountUserServiceImpl extends ServiceImpl<AccountUserMapper, AccountUser> implements IAccountUserService {

    @Override
    public AccountUser findOneUserByName(String username) {
        return this.lambdaQuery().eq(AccountUser::getUsername, username).one();
    }
}
