package com.lytfly.demo.account.service.impl;

import com.lytfly.demo.account.entity.SiteMessage;
import com.lytfly.demo.account.mapper.SiteMessageMapper;
import com.lytfly.demo.account.service.ISiteMessageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author liuyuantao
 * @since 2021年07月10日 16时21分
 */
@Service
public class SiteMessageServiceImpl extends ServiceImpl<SiteMessageMapper, SiteMessage> implements ISiteMessageService {

}
