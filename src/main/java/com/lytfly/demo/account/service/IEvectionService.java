package com.lytfly.demo.account.service;

import com.lytfly.demo.account.entity.Evection;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuyuantao
 * @since 2021年07月10日 16时22分
 */
public interface IEvectionService extends IService<Evection> {


    /**
     * 设置流程变量
     * @param id
     * @return
     */
    public Map<String, Object> setvariables(Integer id);


    /**
     * 整个流程开始时需要执行的任务
     * @param id
     */
    public void startRunTask(Integer id);


    /**
     * 整个流程结束需要执行的任务
     * @param id
     */
    public void endRunTask(Integer id);

}
