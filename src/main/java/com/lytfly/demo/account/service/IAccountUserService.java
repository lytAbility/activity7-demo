package com.lytfly.demo.account.service;

import com.lytfly.demo.account.entity.AccountUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuyuantao
 * @since 2021年07月10日 16时20分
 */
public interface IAccountUserService extends IService<AccountUser> {

    AccountUser findOneUserByName(String username);
}
