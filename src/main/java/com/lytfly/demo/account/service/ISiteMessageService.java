package com.lytfly.demo.account.service;

import com.lytfly.demo.account.entity.SiteMessage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuyuantao
 * @since 2021年07月10日 16时21分
 */
public interface ISiteMessageService extends IService<SiteMessage> {

}
