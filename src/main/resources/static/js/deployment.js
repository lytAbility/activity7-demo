var app = new Vue({
    el:"#app",
    data:{
        flowList:[]
    },
    created:function () {
        //调用方法
        this.findAll();
    },
    methods:{
        //到后台获取列表数据
        findAll:function () {
            axios.get('/account/flowInfo/findAll')
                .then(function(result){
                    app.flowList = result.data;
                });
        },
        deploy:function (id) {
            var vm=this;
            axios.put('/account/flowInfo/deployment/'+id)
                .then(function(result){
                   var data = result.data
                    if(data == 1 ){
                        alert('部署成功！');
                    }else if(data ==2){
                        alert('已经部署过！');
                    }else{
                        alert('部署失败！！')
                    }
                    vm.findAll();
                });
        }
    }
});