package com.lytfly.test;

import com.lytfly.demo.WebApplication;

import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.HistoryService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricActivityInstanceQuery;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.repository.ProcessDefinitionQuery;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {WebApplication.class})
@Slf4j
public class BpmnDemoTest {

    @Autowired
    RepositoryService repositoryService;

    @Autowired
    RuntimeService runtimeService;

    @Autowired
    TaskService taskService;

    @Autowired
    HistoryService historyService;

    @Test
    public void initDb() {

    }

    /**
     * 流程部署
     */
    @Test
    public void deployTest() {
        Deployment deployItem = repositoryService.createDeployment()
                .addClasspathResource("bpmn/holiday.bpmn20.xml")
                .name("请假申请20210709")
                .deploy();
        log.info("流程部署ID：{}", deployItem.getId());
        log.info("流程部署名称：{}", deployItem.getName());
    }

    /**
     * 启动一个实例
     * 启动一个流程表示发起一个新的出差申请单，这就相当于java类与java对象的关系，类定义好后需要new创建一个对象使用，当然可以new多个对象。对于请出差申请流程，张三发起一个出差申请单需要启动一个流程
     * 实例，出差申请单发起一个出差单也需要启动一个流程实例。
     */
    @Test
    public void startProcess() {
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("holiday");
        log.info("流程定义id：" + processInstance.getProcessDefinitionId());
        log.info("流程实例id：" + processInstance.getId());
        log.info("当前活动Id：" + processInstance.getActivityId());

    }

    /**
     * 查询任务
     * 流程启动后，任务的负责人就可以查询自己当前需要处理的任务，查询出来的任务都是该用户的待办任务。
     */
    @Test
    public void testFindPersonalTaskList() {
        String assignee = "lyt";
        List<Task> taskList = taskService.createTaskQuery()
                .processDefinitionKey("holiday") //流程Key
                .taskAssignee(assignee)//只查询该任务负责人的任务
                .list();
        for (Task task : taskList) {
            log.info("流程实例id：" + task.getProcessInstanceId());
            log.info("任务id：" + task.getId());
            log.info("任务负责人：" + task.getAssignee());
            log.info("任务名称：" + task.getName());
        }

    }

    /**
     * 查询任务
     * 任务负责人查询待办任务，选择任务进行处理，完成任务。
     */
    @Test
    public void testHandleTask() {
        String assignee = "lyt";
        String processKey = "holiday";
        // 根据流程key 和 任务的负责人 查询任务
        // 返回一个任务对象
        Task task = taskService.createTaskQuery()
                //流程Key
                .processDefinitionKey(processKey)
                //要查询的负责人
                .taskAssignee(assignee)
                .singleResult();
        // 完成任务,参数：任务id
        taskService.complete(task.getId());
    }

    /**
     * 查询流程定义
     */
    @Test
    public void queryProcessDefinition() {
        String processKey = "holiday";
        // 得到ProcessDefinitionQuery 对象
        ProcessDefinitionQuery processDefinitionQuery =
                repositoryService.createProcessDefinitionQuery();
        // 查询出当前所有的流程定义
        // 条件：processDefinitionKey = processKey
        // orderByProcessDefinitionVersion 按照版本排序
        // desc倒叙
        // list 返回集合
        List<ProcessDefinition> definitionList =
                processDefinitionQuery.processDefinitionKey(processKey)
                        .orderByProcessDefinitionVersion()
                        .desc()
                        .list();
        // 输出流程定义信息
        for (ProcessDefinition processDefinition : definitionList) {
            log.info("流程定义 id=" + processDefinition.getId());
            log.info("流程定义 name=" + processDefinition.getName());
            log.info("流程定义 key=" + processDefinition.getKey());
            log.info("流程定义 Version=" + processDefinition.getVersion());
            log.info("流程部署ID =" + processDefinition.getDeploymentId());
        }
    }

    /**
     * 流程删除
     * 说明：
     * 1) 使用repositoryService删除流程定义，历史表信息不会被删除
     * 2) 如果该流程定义下没有正在运行的流程，则可以用普通删除。
     * 3) 如果该流程定义下存在已经运行的流程，使用普通删除报错，可用级联删除方法将流程及相关记录全部删除。
     * 4) 先删除没有完成流程节点，最后就可以完全删除流程定义信息
     * 注：项目开发中级联删除操作一般只开放给超级管理员使用.
     */
    @Test
    public void deleteDeployment() {
        // 流程部署id
        String deploymentId = "107931c4-e224-11eb-9eb4-005056c00008";
        //删除流程定义，如果该流程定义已有流程实例启动则删除时出错
        //repositoryService.deleteDeployment(deploymentId);
        //设置true 级联删除流程定义，即使该流程有流程实例启动也可以删除，设置为false非级别删除方式，如果流程
        repositoryService.deleteDeployment(deploymentId, true);
    }

    /**
     * 查看历史信息
     */
    @Test
    public void findHistoryInfo() {
        // 获取actinst表的查询对象
        HistoricActivityInstanceQuery instanceQuery =
                historyService.createHistoricActivityInstanceQuery();
        // 查询actinst表，条件：根据 InstanceId 查询
        // instanceQuery.processInstanceId("2501");
        // 查询actinst表，条件：根据 DefinitionId 查询
        instanceQuery.processDefinitionId("holiday:1:f6fb6cb7-e075-11eb-ae78-005056c00008");
        // 增加排序操作,orderByHistoricActivityInstanceStartTime 根据开始时间排序 asc 升序
        instanceQuery.orderByHistoricActivityInstanceStartTime().asc();
        // 查询所有内容
        List<HistoricActivityInstance> activityInstanceList = instanceQuery.list();
        // 输出
        for (HistoricActivityInstance hi : activityInstanceList) {
            log.info(hi.getActivityId());
            log.info(hi.getActivityName());
            log.info(hi.getProcessDefinitionId());
            log.info(hi.getProcessInstanceId());
            log.info("<==========================>");
        }
    }


}
