package com.lytfly.test;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.LocalDateTimeUtil;
import com.lytfly.demo.WebApplication;
import com.lytfly.demo.account.entity.Evection;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.HistoryService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 添加变量演示
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {WebApplication.class})
@Slf4j
public class BpmnProcessVarDemoTest {

    @Autowired
    RepositoryService repositoryService;

    @Autowired
    RuntimeService runtimeService;

    @Autowired
    TaskService taskService;

    @Autowired
    HistoryService historyService;


    /**
     * 流程部署
     */
    @Test
    public void deployTest() {
        Deployment deployItem = repositoryService.createDeployment()
                .addClasspathResource("bpmn/evection.bpmn20.xml")
                .name("出差申请20210710")
                .key("evection0710")
                .deploy();
        log.info("流程部署ID：{}", deployItem.getId());
        log.info("流程部署名称：{}", deployItem.getName());
    }

    /**
     * 启动一个实例
     * 启动一个流程表示发起一个新的出差申请单，这就相当于java类与java对象的关系，类定义好后需要new创建一个对象使用，当然可以new多个对象。对于请出差申请流程，张三发起一个出差申请单需要启动一个流程
     * 实例，出差申请单发起一个出差单也需要启动一个流程实例。
     */
    @Test
    public void startProcess() {
        // 流程定义key
        String key = "evection";
        // 创建变量集合
        Map<String, Object> map = new HashMap<>();
        // 设置assignee的取值，用户可以在界面上设置流程的执行
        map.put("assignee0", "张三2001");
        map.put("assignee1", "李经理2001");
        map.put("assignee2", "王总经理2001");
        map.put("assignee3", "赵财务2001");
        // 启动流程实例，并设置流程变量的值（把map传入）
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()
                .processDefinitionKey(key)
                .singleResult();
        ProcessInstance processInstance = runtimeService.startProcessInstanceById(processDefinition.getId(), map);
        // 输出
        log.info("流程实例名称=" + processInstance.getName());
        log.info("流程定义id==" + processInstance.getProcessDefinitionId());
    }


    /**
     * 启动一个实例
     * 启动一个流程表示发起一个新的出差申请单，这就相当于java类与java对象的关系，类定义好后需要new创建一个对象使用，当然可以new多个对象。对于请出差申请流程，张三发起一个出差申请单需要启动一个流程
     * 实例，出差申请单发起一个出差单也需要启动一个流程实例。
     */
    @Test
    public void startProcessAndVar() {
        // 流程定义key
        String key = "evection";
        // 创建变量集合
        Map<String, Object> map = new HashMap<>();
        // 创建出差pojo对象
        Evection evection = new Evection();
        evection.setId(2001);
        // 设置出差天数
        evection.setNum(2d);
        evection.setReason("拜访客户");
        LocalDate now = LocalDate.now();
        evection.setBeginDate(now);
        evection.setEvectionName("2021年7月10日中旬拜访客户");
        // 定义流程变量，把出差pojo对象放入map
        map.put("evection", evection);
        // 设置assignee的取值，用户可以在界面上设置流程的执行
        map.put("assignee0", "张三2001");
        map.put("assignee1", "李经理2001");
        map.put("assignee2", "王总经理2001");
        map.put("assignee3", "赵财务2001");
        // 启动流程实例，并设置流程变量的值（把map传入）
        ProcessInstance processInstance = runtimeService
                .startProcessInstanceByKey(key, Convert.toStr(evection.getId()), map);
        //.startProcessInstanceByKey(key, map);
        // 输出
        log.info("流程实例名称=" + processInstance.getName());
        log.info("流程定义id==" + processInstance.getProcessDefinitionId());
    }

    @Test
    public void doProcess() {
        // 流程定义key
        String key = "evection";
        String taskAssignee = "张三2001";
        Task task = taskService.createTaskQuery().processDefinitionKey(key)
                .taskAssignee(taskAssignee)
                .singleResult();
        if (task != null) {
            // 创建变量集合
            Map<String, Object> map = new HashMap<>();
            // 创建出差pojo对象
            Evection evection = new Evection();
            evection.setId(2001);
            // 设置出差天数
            evection.setNum(5d);
            evection.setReason("拜访客户");
            LocalDate now = LocalDate.now();
            evection.setBeginDate(now);
            evection.setEvectionName("2021年7月10日中旬拜访客户");
            // 定义流程变量，把出差pojo对象放入map
            map.put("evection", evection);
            taskService.complete(task.getId(), map);
        }
    }
    @Test
    public void doProcess2() {
        // 流程定义key
        String key = "evection";
        String taskAssignee = "王总经理2001";
        Task task = taskService.createTaskQuery().processDefinitionKey(key)
                .taskAssignee(taskAssignee)
                .singleResult();
        if (task != null) {
            taskService.complete(task.getId());
        }
    }
}
