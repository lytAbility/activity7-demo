package com.lytfly.test;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.core.exceptions.MybatisPlusException;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.builder.GeneratorBuilder;
import com.baomidou.mybatisplus.generator.config.converts.MySqlTypeConvert;
import com.baomidou.mybatisplus.generator.config.querys.MySqlQuery;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.baomidou.mybatisplus.generator.fill.Column;
import com.baomidou.mybatisplus.generator.keywords.MySqlKeyWordsHandler;

import java.io.File;
import java.util.Scanner;


public class CodeGenerator {

    static final String PROJECT_PATH = FileUtil.getCanonicalPath(new File(""));

    /**
     * <p>
     * 读取控制台内容
     * </p>
     */
    public static String scanner(String tip) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入" + tip + "：");
        if (scanner.hasNext()) {
            String ipt = scanner.next();
            if (StrUtil.isNotBlank(ipt)) {
                return ipt;
            }
        }
        throw new MybatisPlusException("请输入正确的" + tip + "！");
    }

    public static void main(String[] args) {
        // 代码生成器
        DataSourceConfig dataSourceConfig = new DataSourceConfig.Builder("jdbc:mysql://localhost:3306/activiti_demo?useUnicode=true&useSSL=false&characterEncoding=utf8", "root", "root")
                .typeConvert(new MySqlTypeConvert())
                .keyWordsHandler(new MySqlKeyWordsHandler())
                .dbQuery(new MySqlQuery())
                .build();

        AutoGenerator mpg = new AutoGenerator(dataSourceConfig);
        GlobalConfig gc = GeneratorBuilder.globalConfigBuilder()
                .outputDir(PROJECT_PATH + "/src/main/java")
                .author("liuyuantao")
                .openDir(true)
                .commentDate("yyyy年MM月dd日 HH时mm分")
                .build();
        mpg.global(gc);
        // 包配置
        PackageConfig pc = new PackageConfig.Builder()
                .moduleName(scanner("模块名"))
                .parent("com.lytfly.demo")
                .xml(PROJECT_PATH + "/src/main/resources/mapper")
                .build();
        mpg.packageInfo(pc);


        // 配置模板
        TemplateConfig templateConfig = new TemplateConfig.Builder()
                // 如果模板引擎是freemarker
                .mapperXml("/templates/mapper.xml")
                .build();
        mpg.template(templateConfig);
        // 策略配置
        StrategyConfig strategy = new StrategyConfig.Builder()
                //.addTablePrefix(pc.getModuleName() + "_")
                .addInclude(scanner("表名"))
                .entityBuilder()
                .columnNaming(NamingStrategy.underline_to_camel)
                .naming(NamingStrategy.underline_to_camel)
                .enableLombok()
                .enableChainModel()
                .enableSerialVersionUID()
                .enableTableFieldAnnotation()
                .enableActiveRecord()
                .addTableFills(new Column("create_time", FieldFill.INSERT))
                .addTableFills(new Column("update_time", FieldFill.INSERT_UPDATE))
                .controllerBuilder()
                .enableRestStyle()
                .mapperBuilder()
                .enableBaseColumnList()
                .enableBaseResultMap()
                .serviceBuilder()
                .build();
        mpg.strategy(strategy);

        mpg.execute(new FreemarkerTemplateEngine());
    }

}