package com.lytfly.test;

import com.lytfly.demo.WebApplication;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.HistoryService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricActivityInstanceQuery;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.repository.ProcessDefinitionQuery;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * 包含业务key演示
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {WebApplication.class})
@Slf4j
public class BpmnBusinessKeyDemoTest {

    @Autowired
    RepositoryService repositoryService;

    @Autowired
    RuntimeService runtimeService;

    @Autowired
    TaskService taskService;

    @Autowired
    HistoryService historyService;


    /**
     * 启动一个实例
     * 启动一个流程表示发起一个新的出差申请单，这就相当于java类与java对象的关系，类定义好后需要new创建一个对象使用，当然可以new多个对象。对于请出差申请流程，张三发起一个出差申请单需要启动一个流程
     * 实例，出差申请单发起一个出差单也需要启动一个流程实例。
     */
    @Test
    public void startProcess() {
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("holiday", "1001");
        log.info("流程定义id：" + processInstance.getProcessDefinitionId());
        log.info("流程实例id：" + processInstance.getId());
        log.info("当前活动Id：" + processInstance.getActivityId());

    }

    @Test
    public void queryProcessInstance() {
        // 流程定义key
        String processDefinitionKey = "holiday";

        List<ProcessInstance> list = runtimeService
                .createProcessInstanceQuery()
                .processDefinitionKey(processDefinitionKey)
                .list();
        for (ProcessInstance processInstance : list) {
            log.info("----------------------------");
            log.info("流程实例id：" + processInstance.getProcessInstanceId());
            log.info("业务id：" + processInstance.getBusinessKey());
            log.info("所属流程定义id：" + processInstance.getProcessDefinitionId());
            log.info("是否执行完成：" + processInstance.isEnded());
            log.info("是否暂停：" + processInstance.isSuspended());
            log.info("当前活动标识：" + processInstance.getActivityId());
        }
    }

    /**
     * 全部流程实例挂起与激活
     */
    @Test
    public void suspendAllProcessInstance() {
        String processKey = "holiday";
        // 查询流程定义的对象
        ProcessDefinition processDefinition =
                repositoryService.createProcessDefinitionQuery()
                        .processDefinitionKey(processKey)
                        .singleResult();
        // 得到当前流程定义的实例是否都为暂停状态
        boolean suspended = processDefinition.isSuspended();
        // 流程定义id
        String processDefinitionId = processDefinition.getId();
        // 判断是否为暂停
        if (suspended) {
            // 如果是暂停，可以执行激活操作 ,参数1 ：流程定义id ，参数2：是否激活，参数3：激活时间
            repositoryService.activateProcessDefinitionById(processDefinitionId,
                    true,
                    null
            );
            log.info("流程定义：" + processDefinitionId + ",已激活");
        } else {
            // 如果是激活状态，可以暂停，参数1 ：流程定义id ，参数2：是否暂停，参数3：暂停时间
            repositoryService.suspendProcessDefinitionById(processDefinitionId,
                    true,
                    null);
            log.info("流程定义：" + processDefinitionId + ",已挂起");
        }
    }

    /**
     * 单个流程实例挂起与激活
     * 操作流程实例对象，针对单个流程执行挂起操作，某个流程实例挂起则此流程不再继续执行，完成该流程实例的当前任务将报异常。
     */
    @Test
    public void suspendSingleProcessInstance() {
        //act_ru_execution表的ID
        String processInstanceId = "189bbbf6-e08a-11eb-8284-005056c00008";
        // 查询流程定义的对象
        ProcessInstance processInstance = runtimeService.
                createProcessInstanceQuery().
                processInstanceId(processInstanceId).
                singleResult();
        // 得到当前流程定义的实例是否都为暂停状态
        boolean suspended = processInstance.isSuspended();
        // 流程定义id
        String processDefinitionId = processInstance.getId();
        // 判断是否为暂停
        if (suspended) {
            // 如果是暂停，可以执行激活操作 ,参数：流程定义id
            runtimeService.activateProcessInstanceById(processDefinitionId);
            log.info("流程定义：" + processDefinitionId + ",已激活");
        } else {
            // 如果是激活状态，可以暂停，参数：流程定义id
            runtimeService.suspendProcessInstanceById(processDefinitionId);
            log.info("流程定义：" + processDefinitionId + ",已挂起");
        }
    }

    /**
     * 测试完成个人任务
     */
    @Test
    public void completeTask() {
        String processInstanceId = "189bbbf6-e08a-11eb-8284-005056c00008";
        String taskAssignee = "lyt";
        // 完成任务,参数：流程实例id,完成taskAssignee的任务
        Task task = taskService.createTaskQuery()
                .processInstanceId(processInstanceId)
                .taskAssignee(taskAssignee)
                .singleResult();
        log.info("流程实例id=" + task.getProcessInstanceId());
        log.info("任务Id=" + task.getId());
        log.info("任务负责人=" + task.getAssignee());
        log.info("任务名称=" + task.getName());
        taskService.complete(task.getId());
    }

    /**
     * 需求： 在 activiti 实际应用时，查询待办任务可能要显示出业务系统的一些相关信息。
     * 比如：查询待审批出差任务列表需要将出差单的日期、 出差天数等信息显示出来。
     * 出差天数等信息在业务系统中存在，而并没有在 activiti 数据库中存在，所以是无法通过 activiti 的 api 查询到出差天数等信息。
     * 实现： 在查询待办任务时，通过 businessKey（业务标识 ）关联查询业务系统的出差单表，查询出出差天数等信息。
     */
    @Test
    public void findProcessInstance() {
        String definitionKey = "holiday";
        String taskAssignee = "bmld";
        // 查询流程定义的对象
        Task task = taskService.createTaskQuery()
                .processDefinitionKey(definitionKey)
                .taskAssignee(taskAssignee)
                .singleResult();
        // 使用task对象获取实例id
        String processInstanceId = task.getProcessInstanceId();
        // 使用实例id，获取流程实例对象
        ProcessInstance processInstance = runtimeService.createProcessInstanceQuery()
                .processInstanceId(processInstanceId)
                .singleResult();
        // 使用processInstance，得到 businessKey
        String businessKey = processInstance.getBusinessKey();
        log.info("businessKey==" + businessKey);
        log.info("拿到businessKey后就可以去业务数据库去查看业务数据了!");
    }


}
